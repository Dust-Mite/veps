﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace VEPS
{
    public partial class addTask : Form
    {
        public addTask()
        {
            InitializeComponent();
        }

        private void addTask_Load(object sender, EventArgs e) // Gets the projects data to allow for user to select which project the new task will be part of
        {
            XmlReaderSettings rs = new XmlReaderSettings(); // Sets the settings for the api XML calls, its needed for something but cant rememeber
            rs.DtdProcessing = DtdProcessing.Ignore; // Having issue with DTD processing with API so disabled it

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//project.php?tag=projects", rs))
            {
                reader1.ReadToFollowing("Project"); // Reads to first instance of Project
                do
                {
                    reader1.ReadToFollowing("id"); // Moves to first instance of id in xml sheet after Project has been found
                    string id = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name"); // Moves first instant of Name after Project has been found
                    string name = reader1.ReadElementContentAsString();

                    cbProjects.Items.Add(new comboBoxItems(name, id)); // Adds item to projects drop down with hidden value as ID

                } while (reader1.ReadToFollowing("Project")); // Reads to each Project inside the xml document

                reader1.Close(); // Closes the stream to clean up
            }
        }

        private void cbProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int jj = cbCategories.Items.Count; jj != 0; jj--) // Scrolls through the current items in cbCategories to make sure it is empty to add new items into
            {
                cbCategories.Items.RemoveAt(jj - 1);
            }

            int project = Int32.Parse(((comboBoxItems)cbProjects.SelectedItem).HiddenValue); // Gets the selected project id from the hidden value in 

            XmlReaderSettings rs = new XmlReaderSettings(); // Sets the settings for the api XML calls, its needed for something but cant rememeber
            rs.DtdProcessing = DtdProcessing.Ignore; // Having issue with DTD processing with API so disabled it

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//category.php?tag=category&column=Project_id&name=" + project, rs))
            {
                reader1.ReadToFollowing("Category");  // Reads to first instance of Category
                do
                {
                    reader1.ReadToFollowing("id"); // Moves to first instance of id in xml sheet after Project has been found
                    string id = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name"); // Moves first instant of Name after Project has been found
                    string name = reader1.ReadElementContentAsString();

                    cbCategories.Items.Add(new comboBoxItems(name, id)); // Adds item to categories drop down with hidden value as ID

                } while (reader1.ReadToFollowing("Category")); // Reads to each Category inside the xml document

                reader1.Close(); // Closes the stream to clean up
            }

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//users.php?tag=all", rs))
            {
                reader1.ReadToFollowing("User");

                do
                {
                    reader1.ReadToFollowing("id"); // Moves to first instance of id in xml sheet after Project has been found
                    string id = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name"); // Moves first instant of Name after Project has been found
                    string name = reader1.ReadElementContentAsString();

                    clbUsers.Items.Add(new comboBoxItems(name, id)); // Adds item to Users check box list with hidden value as ID
                 
                } while (reader1.ReadToFollowing("User"));

                reader1.Close(); // Closes the stream to clean up
            }
        }

        private void cbCategories_SelectedIndexChanged(object sender, EventArgs e)
        {
            int items = 0;
            int category = Int32.Parse(((comboBoxItems)cbCategories.SelectedItem).HiddenValue);

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;

           
            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//task.php?tag=singleTask&column=id&name=" + category, rs))
            {
                reader1.ReadToFollowing("Task");
                do
                {

                    items++;

                } while (reader1.ReadToFollowing("Task"));

                reader1.Close(); // Closes the stream to clean up
            }


            if (items <= 10)
            {
                txtTaskName.Enabled = true;
                cbCompleted.Enabled = true;
                txtDateDue.Enabled = true;
                clbUsers.Enabled = true;
                txtNotes.Enabled = true;

                lblCategory.Visible = false;
            }
            else
            {
                txtTaskName.Enabled = false;
                cbCompleted.Enabled = false;
                txtDateDue.Enabled = false;
                clbUsers.Enabled = false;
                txtNotes.Enabled = false;

                lblCategory.Visible = true;
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            int category = Int32.Parse(((comboBoxItems)cbCategories.SelectedItem).HiddenValue);
            int project = Int32.Parse(((comboBoxItems)cbProjects.SelectedItem).HiddenValue);
            string users = null;

            foreach (object itemChecked in clbUsers.CheckedItems)
            {
                if (users == null)
                {
                    users = ((comboBoxItems)itemChecked).HiddenValue;
                }
                else if (users != null)
                {
                    users = users + "O" + ((comboBoxItems)itemChecked).HiddenValue;
                }
            }

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;

            HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(@"http://www.volatileelement.co.uk/api//task.php?tag=new&name=" + txtTaskName.Text + "&completed=" + cbCompleted.Checked + "&dateDue=" + txtDateDue.Text + "&users=" + users + "&notes=" + txtNotes.Text + "&project=" + project + "&category=" + category + "&archived=False");
            objRequest.Method = WebRequestMethods.Http.Get;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            StreamReader reader = new StreamReader(objResponse.GetResponseStream());

            reader.Close(); // Closes the stream to clean up

           

            if (System.Windows.Forms.Application.OpenForms["mainForm"] != null)
            {
                (System.Windows.Forms.Application.OpenForms["mainForm"] as mainForm).prePopulator();
            }


            this.Close();
        }     
    }
}
