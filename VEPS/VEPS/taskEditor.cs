﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace VEPS
{
    public partial class taskEditor : Form
    {
        int idP;
        string projectID, categoryID, users = null;
        bool archived = false;

        public taskEditor(int id)
        {
            InitializeComponent();

            idP = id;

            populator();

        }

        private void populator()
        {
            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//task.php?tag=singleTask&column=id&name=" + idP, rs))
            {
                reader1.ReadToFollowing("Task");
                do
                {
                    reader1.ReadToDescendant("id");
                    txtTaskID.Text = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Name");
                    txtTaskName.Text = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Completed");
                    cBCompleted.Checked = reader1.ReadElementContentAsBoolean();

                    reader1.ReadToFollowing("Date_Due");
                    txtDateDue.Text = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Users_id");
                    users = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Notes");
                    txtNotes.Text = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Project_id");
                    projectID = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Category_id");
                    categoryID = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Archived");
                    archived = reader1.ReadElementContentAsBoolean();

                } while (reader1.ReadToFollowing("Task"));

                reader1.Close(); // Closes the stream to clean up
            }


            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//project.php?tag=project&column=id&name=" + projectID, rs))
            {
                reader1.ReadToFollowing("Project");
                do
                {
                    reader1.ReadToFollowing("id");
                    txtProjectID.Text = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name");
                    txtProjectName.Text = reader1.ReadElementContentAsString();

                } while (reader1.ReadToFollowing("Project"));

                reader1.Close(); // Closes the stream to clean up
            }

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//category.php?tag=categories", rs))
            {
                reader1.ReadToFollowing("Category");
                do
                {
                    reader1.ReadToFollowing("id");
                    string id = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name");
                    string name = reader1.ReadElementContentAsString();

                    cbCategoryName.Items.Add(new comboBoxItems(name, id));

                    //reader1.ReadToDescendant("Name");
                    //cbCategoryID.Items.Add(reader1.ReadElementContentAsString());
                    
                } while (reader1.ReadToFollowing("Category"));

                reader1.Close(); // Closes the stream to clean up
            }

            //project = Int32.Parse(((comboBoxItems)cbProjects.SelectedItem).HiddenValue);

            int hold = 0;
            foreach (object items in cbCategoryName.Items)
            {
                if (categoryID == ((comboBoxItems)cbCategoryName.Items[hold]).HiddenValue)
                {
                    cbCategoryName.SelectedIndex = cbCategoryName.Items.IndexOf(items);
                }

                hold++;
            }

            //using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//category.php?tag=category&column=id&name=" + categoryID, rs))
            //{
            //    reader1.ReadToFollowing("Category");
            //    do
            //    {
            //        reader1.ReadToDescendant("Name");
            //        cbCategoryName.SelectedIndex = cbCategoryName.Items.IndexOf(reader1.ReadElementContentAsString());
                    
            //    } while (reader1.ReadToFollowing("Category"));
            //}


            string[] usersSplits = users.Split('O');

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//users.php?tag=all", rs))
            {
                reader1.ReadToFollowing("User");

                do
                {
                    reader1.ReadToFollowing("uid");
                    string id = reader1.ReadElementContentAsString().ToString();

                    reader1.ReadToFollowing("Name");
                    string Name = reader1.ReadElementContentAsString();

                    Boolean tried = false;
                    foreach (string userSplit in usersSplits)
                    {
                        if (id == userSplit)
                        {
                            clbUsers.Items.Add((new comboBoxItems(Name, id)), true);
                            tried = true;
                        }
                    }

                    if (tried == false)
                    {
                        clbUsers.Items.Add((new comboBoxItems(Name, id)), false);
                    }

                } while (reader1.ReadToFollowing("User"));

                reader1.Close(); // Closes the stream to clean up
            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            int category = 0;
            string users = null;

            category = Int32.Parse(((comboBoxItems)cbCategoryName.SelectedItem).HiddenValue);

            foreach (object itemChecked in clbUsers.CheckedItems)
            {
                if (users == null)
                {
                    users = ((comboBoxItems)itemChecked).HiddenValue;
                }
                else if (users != null)
                {
                    users = users + "O" + ((comboBoxItems)itemChecked).HiddenValue;
                }
            }

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;

            HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(@"http://www.volatileelement.co.uk/api//task.php?tag=update&name=" + txtTaskName.Text + "&completed=" + cBCompleted.Checked + "&dateDue=" + txtDateDue.Text + "&users=" + users + "&notes=" + txtNotes.Text + "&project=" + txtProjectID.Text + "&category=" + category + "&archived=" + archived + "&id=" + idP);
            objRequest.Method = WebRequestMethods.Http.Get;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            StreamReader reader = new StreamReader(objResponse.GetResponseStream());

            reader.Close(); // Closes the stream to clean up


            //using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//category.php?tag=category&column=Name&name=" + cbCategoryName.SelectedItem, rs))
            //{
            //    reader1.ReadToFollowing("Category");
            //    do
            //    {
            //        reader1.ReadToDescendant("id");
            //        category = reader1.ReadElementContentAsInt();

            //    } while (reader1.ReadToFollowing("Category"));
            //}


            //using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//project.php?tag=project&column=Name&name=" + txtProjectName.Text, rs))
            //{
            //    reader1.ReadToFollowing("Project");
            //    do
            //    {
            //        reader1.ReadToDescendant("id");
            //        project = reader1.ReadElementContentAsInt();

            //    } while (reader1.ReadToFollowing("Project"));
            //}

            

                //using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//users.php?tag=specific&column=name&name=" + itemChecked.ToString(), rs))
                //{
                //    reader1.ReadToFollowing("User");
                //    do
                //    {
                //        reader1.ReadToFollowing("uid");

                //        if (users == null)
                //        {
                //            users = reader1.ReadElementContentAsInt() + "";
                //        }
                //        else if (users != null)
                //        {
                //            users = users + "O" + reader1.ReadElementContentAsInt();
                //        }

                //    } while (reader1.ReadToFollowing("User"));
                //}
            

            

            //String str = VEPS.Properties.Settings.Default.databaseConnectionString.ToString();
            //MySqlConnection con = null;
            ////MySqlDataReader Object
            //MySqlDataReader reader = null;
            //try
            //{
            //    con = new MySqlConnection(str);
            //    con.Open(); //open the connection
            //    //We will need to SELECT all or some columns in the table
            //    //via this command

            //    String cmdText1 = "SELECT id FROM `VeRT_Categories` WHERE `Name` = \"" + cbProjectID.SelectedItem + "\"";
            //    //MessageBox.Show(cmdText1);
            //    MySqlCommand cmd1 = new MySqlCommand(cmdText1, con);
            //    reader = cmd1.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        //addGroupBox(reader.GetString(1));


            //        //cbProjectID.Items.Add(reader.GetString(0));

            //        //txtProjectID.Text = reader.GetString(0);

            //        category = reader.GetInt32(0);

            //        //MessageBox.Show(category + "");


            //    }

            //    reader.Close();


            //    String cmdText2 = "SELECT id FROM `VeRT_Projects` WHERE `Name` = \"" + txtProjectID.Text + "\"";
            //    //MessageBox.Show(cmdText2);
            //    MySqlCommand cmd2 = new MySqlCommand(cmdText2, con);
            //    reader = cmd2.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        //addGroupBox(reader.GetString(1));


            //        //cbProjectID.Items.Add(reader.GetString(0));

            //        //txtProjectID.Text = reader.GetString(0);

            //        project = reader.GetInt32(0);

            //        //MessageBox.Show(project + "");


            //    }

            //    reader.Close();

            //    foreach (object itemChecked in clbUsers.CheckedItems)
            //    {
            //        String cmdTextFor = "SELECT id FROM `VeRT_Users` WHERE `Name` = \"" + itemChecked.ToString() + "\"";
            //        //MessageBox.Show(cmdTextFor);
            //        MySqlCommand cmdFor = new MySqlCommand(cmdTextFor, con);
            //        reader = cmdFor.ExecuteReader(); //execure the reader
            //        /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //        while (reader.Read())
            //        {

            //            if (users == null)
            //            {
            //                users = reader.GetInt32(0) + "";
            //            }
            //            else if (users != null)
            //            {
            //                users = users + "O" + reader.GetInt32(0);
            //            }

            //        }

            //        reader.Close();
            //    }




            //    String cmdText = "UPDATE `VeRT_Tasks` SET `Name`= \"" + txtTaskName.Text + "\",`Completed`= " + cBCompleted.Checked + ",`Date Due`= \"" + txtDateDue.Text + "\",`Users ID`= \"" + users + "\",`Notes`= \"" + txtNotes.Text + "\",`Project id`= \"" + project + "\",`Category id`= \"" + category + "\" WHERE `id` = " + idP;
            //    //MessageBox.Show(cmdText);
            //    MySqlCommand cmd = new MySqlCommand(cmdText, con);
            //    reader = cmd.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //}
            //catch (MySqlException err)
            //{
            //    //Console.WriteLine("Error: " + err.ToString());
            //    MessageBox.Show("Error: " + err.ToString());
            //}
            //finally
            //{
            //    if (reader != null)
            //    {
            //        reader.Close();
            //    }
            //    if (con != null)
            //    {
            //        con.Close(); //close the connection
            //    }
            //} //remember to close the connection after accessing the database


            if (System.Windows.Forms.Application.OpenForms["mainForm"] != null)
            {
                (System.Windows.Forms.Application.OpenForms["mainForm"] as mainForm).prePopulator();
            }


            this.Close();
        }

        private void cmdClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbCategoryName_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
