﻿//using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
//using Google.GData.Client;
//using Google.GData.Extensions;
//using Google.GData.Spreadsheets;

namespace VEPS
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();

            gB = new GroupBox[100];
            tB = new TextBox[100];
            tBDate = new TextBox[100];
            cB = new CheckBox[100];
            bN = new Button[100];
            items = new int[100];
            bNAdd = new Button[100];

        }

        //public WorksheetFeed wsFeed;
        //public SpreadsheetsService service = new SpreadsheetsService("MySpreadsheetIntegration-v1");

        public string[,] workbookCells;

        public string cellEdits;

        int buttonpress = 1;
        TextBox[] tB, tBDate;
        GroupBox[] gB;
        int[] items;
        CheckBox[] cB;
        Button[] bN, bNAdd;
        int groupBoxNum = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            projectsLoad();
        }

        private void projectsLoad()
        {
            for (int jj = cbProjects.Items.Count; jj != 0; jj--)
            {
                cbProjects.Items.RemoveAt(jj - 1);
            }

            //HttpWebRequest objRequest = (HttpWebRequest)HttpWebRequest.Create(@"http://api.volatileelement.co.uk/project.php?tag=projects");
            //objRequest.Method = WebRequestMethods.Http.Get;
            //HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            //StreamReader readerWeb1 = new StreamReader(objResponse.GetResponseStream());
            //string tmp1 = readerWeb1.ReadToEnd();

            //FileStream input1 = new FileStream(, FileMode.Open, FileAccess.Read, FileShare.Read);

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;
                      

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//project.php?tag=projects", rs))
            {
                reader1.ReadToFollowing("Project");
                do
                {
                    reader1.ReadToFollowing("id");
                    string id = reader1.ReadElementContentAsString();
                    reader1.ReadToFollowing("Name");
                    string name = reader1.ReadElementContentAsString();
                    
                    cbProjects.Items.Add(new comboBoxItems(name, id));
                } while (reader1.ReadToFollowing("Project"));
            }


            //String str = VEPS.Properties.Settings.Default.databaseConnectionString.ToString();

            //MySqlConnection con = null;
            ////MySqlDataReader Object
            //MySqlDataReader reader = null;
            //try
            //{
            //    con = new MySqlConnection(str);
            //    con.Open(); //open the connection
            //    //We will need to SELECT all or some columns in the table
            //    //via this command
            //    String cmdText = "SELECT * FROM VeRT_Projects";
            //    MySqlCommand cmd = new MySqlCommand(cmdText, con);
            //    reader = cmd.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        //listBox1.Items.Add(reader.GetString(1));


            //        if (reader.GetBoolean(2) == false)
            //        {
            //            cbProjects.Items.Add(reader.GetString(1));
            //        }

            //    }
            //}
            //catch (MySqlException err)
            //{
            //    //Console.WriteLine("Error: " + err.ToString());
            //    MessageBox.Show("Error: " + err.ToString());
            //}
            //finally
            //{
            //    if (reader != null)
            //    {
            //        reader.Close();
            //    }
            //    if (con != null)
            //    {
            //        con.Close(); //close the connection
            //    }
            //} //remember to close the connection after accessing the database

            cbProjects.Enabled = true;
        }

        private void databaseLoad()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("database.xml");
            string xmlcontents = doc.InnerXml;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            prePopulator();

        }

        public void prePopulator()
        {
            foreach (GroupBox gbb in gB)
            {
                if (gbb != null)
                    gbb.Dispose();
            }

            foreach (TextBox tbb in tB)
            {
                if (tbb != null)
                    tbb.Dispose();
            }

            foreach (TextBox tBDateB in tBDate)
            {
                if (tBDateB != null)
                    tBDateB.Dispose();
            }

            foreach (CheckBox cbb in cB)
            {
                if (cbb != null)
                    cbb.Dispose();
            }

            foreach (Button bnn in bN)
            {
                if (bnn != null)
                    bnn.Dispose();
            }

            foreach (Button bnn in bNAdd)
            {
                if (bnn != null)
                    bnn.Dispose();
            }




            gB = new GroupBox[100];
            tB = new TextBox[100];
            tBDate = new TextBox[100];
            cB = new CheckBox[100];
            bN = new Button[100];
            items = new int[100];
            bNAdd = new Button[100];

            groupBoxNum = 0;

            buttonpress = 1;

            populator();
        
        }

        private void populator()
        {

            int project = Int32.Parse(((comboBoxItems)cbProjects.SelectedItem).HiddenValue);

            XmlReaderSettings rs = new XmlReaderSettings();
            rs.DtdProcessing = DtdProcessing.Ignore;

            //using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//project.php?tag=project&column=Name&name=" + (cbProjects.SelectedItem.ToString()), rs))
            //{
            //    reader1.ReadToFollowing("Project");
            //    do
            //    {
            //        reader1.ReadToDescendant("id");
            //        //cbProjects.Items.Add(reader1.ReadElementContentAsString());
            //        project = reader1.ReadElementContentAsInt();
            //    } while (reader1.ReadToFollowing("Project"));
            //}

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//category.php?tag=categoryNonArchived&column=Project_ID&name=" + project, rs))
            {
                reader1.ReadToFollowing("Category");
                do
                {
                    reader1.ReadToDescendant("Name");
                    addGroupBox(reader1.ReadElementContentAsString());

                } while (reader1.ReadToFollowing("Category"));

                reader1.Close(); // Closes the stream to clean up
            }

            

            using (XmlReader reader1 = XmlReader.Create(@"http://www.volatileelement.co.uk/api//task.php?tag=tasksNonArchived", rs))
            {
                reader1.ReadToFollowing("Task");
                do
                {
                    reader1.ReadToDescendant("id");
                    int id = reader1.ReadElementContentAsInt();

                    reader1.ReadToFollowing("Name");
                    string Name = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Completed");
                    bool isDone = reader1.ReadElementContentAsBoolean();

                    reader1.ReadToFollowing("Date_Due");
                    string textDate = reader1.ReadElementContentAsString();

                    reader1.ReadToFollowing("Category_id");
                    int Categoryid = reader1.ReadElementContentAsInt();

                    addTextBox(id, Name, isDone, textDate, Categoryid - 1);

                } while (reader1.ReadToFollowing("Task"));

                reader1.Close(); // Closes the stream to clean up
            }


            //String str = VEPS.Properties.Settings.Default.databaseConnectionString.ToString();
            //MySqlConnection con = null;
            ////MySqlDataReader Object
            //MySqlDataReader reader = null;
            //try
            //{
            //    con = new MySqlConnection(str);
            //    con.Open(); //open the connection
            //    //We will need to SELECT all or some columns in the table
            //    //via this command

            //    String cmdProject = "SELECT id FROM `VeRT_Projects` WHERE `Name` = \"" + (cbProjects.SelectedItem.ToString()) + "\"";
            //    //MessageBox.Show(cmdText);
            //    MySqlCommand cmdProjectCom = new MySqlCommand(cmdProject, con);
            //    reader = cmdProjectCom.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        project = reader.GetInt32(0);

            //    }

            //    reader.Close();



            //    String cmdText = "SELECT * FROM `VeRT_Categories` WHERE `Project id` = " + project;
            //    //MessageBox.Show(cmdText);
            //    MySqlCommand cmd = new MySqlCommand(cmdText, con);
            //    reader = cmd.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        if (reader.GetBoolean(3) == false)
            //        {
            //            addGroupBox(reader.GetString(1));
            //        }

            //        //cbProjects.Items.Add(reader.GetString(1));

            //        //string booler = reader.GetString(2);

            //        //if (booler == "False")
            //        //{
            //        //    addTextBox(listBox1.SelectedIndex, reader.GetString(1), reader.GetString(3), false);
            //        //}
            //        //else if (booler == "True")
            //        //{
            //        //    addTextBox(listBox1.SelectedIndex, reader.GetString(1), reader.GetString(3), true);
            //        //}

            //    }

            //    reader.Close();

            //    String cmdText1 = "SELECT * FROM `VeRT_Tasks` WHERE `Project id` = " + project;
            //    //MessageBox.Show(cmdText1);
            //    MySqlCommand cmd1 = new MySqlCommand(cmdText1, con);
            //    reader = cmd1.ExecuteReader(); //execure the reader
            //    /*The Read() method points to the next record It return false if there are no more records else returns true.*/
            //    while (reader.Read())
            //    {
            //        /*reader.GetString(0) will get the value of the first column of the table myTable because we selected all columns using SELECT * (all); the first loop of the while loop is the first row; the next loop will be the second row and so on...*/
            //        //Console.WriteLine(reader.GetString(0));

            //        //addGroupBox(reader.GetString(1));

            //        if (reader.GetBoolean(8) == false)
            //        {

            //            //addTextBox((reader.GetInt32(7) - 1), reader.GetString(1), reader.GetString(3), reader.GetBoolean(2), reader.GetInt32(0));
            //        }



            //    }

            //}
            //catch (MySqlException err)
            //{
            //    //Console.WriteLine("Error: " + err.ToString());
            //    MessageBox.Show("Error: " + err.ToString());
            //}
            //finally
            //{
            //    if (reader != null)
            //    {
            //        reader.Close();
            //    }
            //    if (con != null)
            //    {
            //        con.Close(); //close the connection
            //    }
            //} //remember to close the connection after accessing the database
        }

        //private void listShow(int sheet)
        //{
        //    WorksheetEntry worksheet = (WorksheetEntry)wsFeed.Entries[sheet];
        //    CellQuery cellQuery = new CellQuery(worksheet.CellFeedLink);

        //    CellFeed cellFeed = service.Query(cellQuery);

        //    int rows = Int32.Parse(cellFeed.RowCount.Value);
        //    int col = Int32.Parse(cellFeed.ColCount.Value);

        //    workbookCells = new string[rows, col];

        //    //textBox2.Text = col + "";

        //    int cellRow = 0;
        //    int cellCol = 0;
        //    foreach (CellEntry cell in cellFeed.Entries)
        //    {

        //        workbookCells[cellRow, cellCol] = cell.InputValue;

        //        if (cellCol > col)
        //        {
        //            cellRow++;
        //        }

        //        cellCol++;

        //    }

        //    textBox2.Text = workbookCells[0, 1];

        //    dataGridView1.DataSource = new Mommo.Data.ArrayDataView(workbookCells);

        //}

        private void button2_Click(object sender, EventArgs e)
        {
            //string[] cells = cellEdits.Split('.');

            //foreach (string cell in cells)
            //{
            //    //textBox1.Text += " + " + cell;

            //    string first = cell.Substring(0, 1);

            //    string second = cell.Substring(2, 1);

            //    textBox1.Text += " + " + first + "&" + second;
            //}

            //cellEdits = null;

            addGroupBox("testing");

            buttonpress += 10;
        }

        private void addGroupBox(string title)
        {

            if (groupBoxNum < 10)
            {
                gB[groupBoxNum] = new GroupBox();

                int x = 25, y = 10;

                gB[groupBoxNum].Location = new System.Drawing.Point(((groupBoxNum) * 260) + y, x);
                gB[groupBoxNum].Size = new Size(200, 200);
                gB[groupBoxNum].Name = "gBox" + groupBoxNum.ToString();
                gB[groupBoxNum].Text = title;
                gB[groupBoxNum].AutoSize = true;

                //this.ResumeLayout(false);
                this.Controls.Add(gB[groupBoxNum]);


                //addTextBox(groupBoxNum, "Words", "18/12/2014", false);
                //addTextBox(groupBoxNum, "Words", "18/12/2014", true);
                //addTextBox(groupBoxNum, "Words", "18/12/2014", false);


                //bN[groupBoxNum] = new Button();

                //bN[groupBoxNum].Location = new System.Drawing.Point(((groupBoxNum) * 260) + 18, gB[groupBoxNum].Height + 10); //(buttonpress - 1) / 10
                //bN[groupBoxNum].Size = new Size(230, 23);
                //bN[groupBoxNum].Name = "cmd" + buttonpress.ToString();
                //bN[groupBoxNum].Text = "Add TextBox";
                //bN[groupBoxNum].Click += new EventHandler(dBEditor_Click);


                //this.Controls.Add(bN[groupBoxNum]);


                this.Refresh();

                textBox1.Text += gB[groupBoxNum].Location.ToString();

                groupBoxNum++;
            }
        }

        private void dBTextAdd_Click(object sender, EventArgs e)
        {
            Button button = sender as Button;

            string holdMe = button.Name.Replace("cmd", "");

            //addTextBox((Convert.ToInt32(holdMe) - 1) / 10, "Words", "18/12/2014", false);

            bN[(Convert.ToInt32(holdMe) - 1) / 10].Location = new System.Drawing.Point(gB[(Convert.ToInt32(holdMe) - 1) / 10].Location.X, gB[(Convert.ToInt32(holdMe) - 1) / 10].Height + 10);

            if (items[(Convert.ToInt32(holdMe) - 1) + 9] == 1)
            {
                bN[(Convert.ToInt32(holdMe) - 1) / 10].Enabled = false;
            }

            textBox1.Text += " + " + holdMe;

            this.Refresh();
        }

        private void dBEditor_Click(object sender, EventArgs e)
        {
            //int id = Convert.ToInt32(sender.ToString().Replace("cmdAdd",""));

            Button button = sender as Button;

            //int id = ;

            taskEditor nF = new taskEditor(Convert.ToInt32(button.Name.Replace("cmdAdd", "")));

            nF.Show();
        }

        private void addTextBox(int itemID, string textName, bool isDone, string textDate, int groupBox)
        {

            int y = 10, textBoxes = 0, textBoxX = 20, checkBoxes = 0;

            textBox1.Text = "";

            while (items[((groupBox) * 10) + (textBoxes)] == 1)
            {
                   textBoxes++;
                   checkBoxes++;

                   textBox1.Text += textBoxes + " + ";
            }

            if (textBoxes < 10)
            {
                tB[((groupBox) * 10) + (textBoxes)] = new TextBox();
                tB[((groupBox) * 10) + (textBoxes)].Location = new System.Drawing.Point(y, textBoxX + ((textBoxes) * 30));
                tB[((groupBox) * 10) + (textBoxes)].Size = new Size(100, 50);
                tB[((groupBox) * 10) + (textBoxes)].Name = "txtName" + (textBoxes).ToString();
                tB[((groupBox) * 10) + (textBoxes)].Text = textName;
                tB[((groupBox) * 10) + (textBoxes)].Enabled = false;

                gB[(groupBox)].Controls.Add(tB[((groupBox) * 10) + (textBoxes)]);

                textBox1.Text += " = Added " + textBoxes;

                items[((groupBox) * 10) + (textBoxes)] = 1;


                cB[((groupBox) * 10) + (checkBoxes)] = new CheckBox();
                cB[((groupBox) * 10) + (checkBoxes)].Location = new System.Drawing.Point(y + 110, textBoxX + ((checkBoxes) * 30));
                cB[((groupBox) * 10) + (checkBoxes)].Size = new Size(20, 20);
                cB[((groupBox) * 10) + (checkBoxes)].Name = "ckbx" + (checkBoxes).ToString();
                cB[((groupBox) * 10) + (checkBoxes)].Checked = isDone;
                cB[((groupBox) * 10) + (checkBoxes)].Enabled = false;

                gB[(groupBox)].Controls.Add(cB[((groupBox) * 10) + (checkBoxes)]);


                tBDate[((groupBox) * 10) + (textBoxes)] = new TextBox();
                tBDate[((groupBox) * 10) + (textBoxes)].Location = new System.Drawing.Point(y + 130, ((textBoxX + (textBoxes) * 30)));
                tBDate[((groupBox) * 10) + (textBoxes)].Size = new Size(75, 50);
                tBDate[((groupBox) * 10) + (textBoxes)].Name = "txtDate" + (textBoxes).ToString();
                tBDate[((groupBox) * 10) + (textBoxes)].Text = textDate;
                tBDate[((groupBox) * 10) + (textBoxes)].Enabled = false;

                gB[(groupBox)].Controls.Add(tBDate[((groupBox) * 10) + (textBoxes)]);


                bNAdd[((groupBox) * 10) + (checkBoxes)] = new Button();

                bNAdd[((groupBox) * 10) + (checkBoxes)].Location = new System.Drawing.Point(y + 210, ((textBoxX + (textBoxes) * 30) - 1)); //(buttonpress - 1) / 10
                bNAdd[((groupBox) * 10) + (checkBoxes)].Size = new Size(20, 22);
                bNAdd[((groupBox) * 10) + (checkBoxes)].Name = "cmdAdd" + itemID;
                bNAdd[((groupBox) * 10) + (checkBoxes)].Text = "+";
                bNAdd[((groupBox) * 10) + (checkBoxes)].Click += new EventHandler(dBEditor_Click);


                gB[(groupBox)].Controls.Add(bNAdd[((groupBox) * 10) + (checkBoxes)]);

                this.Refresh();
            }
        }

        private void cbProjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            prePopulator();
        }

        private void tsmNewTask_Click(object sender, EventArgs e)
        {
            addTask aT = new addTask();
            aT.Show();
        }

    }
}
